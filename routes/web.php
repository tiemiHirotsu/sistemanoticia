<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home.index');
})->name('home');

Route::get('/tecnologia', function(){
    return view('noticias.index');
})->name('noticiasIndex');

Route::get('/tecnologia/titulo-noticia', function(){
    return view('noticias.visualizar');
});

Route::get('/contato', function(){
    return view('contato.contato');
})->name('contato');
