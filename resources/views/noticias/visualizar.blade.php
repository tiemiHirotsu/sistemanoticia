@extends('layouts.app')

@section('titulo', 'Tecnologia')

@section('conteudo')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h2>Tecnologia</h2>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 mx-auto">
            <article class="box-noticia">
                <h2>Titulo Noticia</h2>
                <p>06/05/2019</p>
                <p class="text-center p-5">
                    <img src="http://via.placeholder.com/500x250"  class="img-fluid">
                </p>
                <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit. Architecto, ipsum. Aliquid distinctio cum cupiditate quas nesciunt quis commodi aperiam. Minus reprehenderit ad inventore magnam ipsum praesentium doloremque numquam quae. Rerum!</p>
            </article>
        </div>
    </div>
</div>
    
@endsection