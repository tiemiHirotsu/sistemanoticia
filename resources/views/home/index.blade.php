@extends('layouts.app')

@section('titulo', 'Home')
    
@section('conteudo')
    <div class="container">
        <article class="row">
            <div class="col-md-4">
                <img src="http://via.placeholder.com/400x250"  class="img-fluid">
            </div>
            <div class="col-md-8">
                <h2>Notícia Destaque</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repudiandae voluptatibus quibusdam delectus, rem placeat sapiente expedita culpa neque temporibus deserunt distinctio at debitis nobis earum nam. Exercitationem dolore eos accusantium! Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam tempora unde accusamus, nobis itaque exercitationem dolorum sed non quam, magnam, culpa reprehenderit iusto temporibus officia voluptate illum? Culpa, facilis repudiandae?</p>
            </div>
        </article>
        <div class="row">

            @for($i = 1; $i <= 3; $i++)
            <div class="col-md-4 mt-5">
                <article class="card">
                    <a href="">
                        <img src="http://via.placeholder.com/500x250"  class="img-fluid">
                    </a>
                    <div class="card-body">
                        <h2 class="card-title"><a href="#">Título Noticias</a></h2>
                        <p class="card-text">Lorem ipsum dolor sit amet consectetur, adipisicing elit. Impedit odio illo cupiditate, fugit, doloremque asperiores rem quam quas quae debitis placeat libero, esse modi optio magni. Ratione nihil odit consequatur. Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias debitis laboriosam explicabo consequatur fugit aspernatur, facilis vel, veritatis architecto illum tempore accusantium quisquam amet nihil officiis aliquam at exercitationem cum.</p>
                    </div>
                    <div class="card-footer">
                        30/04/2019
                    </div>
                </article>
            </div>
            @endfor
        </div>
    </div>
@endsection