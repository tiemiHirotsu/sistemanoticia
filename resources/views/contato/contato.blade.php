@extends('layouts.app')

@section('titulo', 'Contato')

@section('conteudo')
    
    <div class="container">
        <div class="row">
            <div class="col-8 mx-auto">
                <h1>Contato</h1>
                <span id="texto">Faça sua sugestão, criticas ou tire suas dúvidas sobre algum conteúdo publicado.</span>
                <form action="#" method="post">
                    <div class="form-group">
                        <label for="nome">Nome: </label>
                        <input class="form-control" type="text" name="nome">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail: </label>
                        <input class="form-control" type="email" name="email">
                    </div>
                    <div class="form-group">
                        <label for="telefone">Telefone: </label>
                        <input class="form-control" type="tel" name="telefone">
                    </div>
                    <div class="form-group">
                        <label for="assunto">Assunto: </label>
                        <input class="form-control" type="text" name="assunto">
                    </div>
                    <div class="form-group">
                        <label for="mensagem">Mensagem: </label>
                        <textarea class="form-control" name="mensagem" rows="3"></textarea>
                    </div>
                    <button type="submit" class="btn btn-danger">Enviar</button>
                </form>
            </div>
        </div>
    </div>

@endsection